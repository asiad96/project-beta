import React, { useState, useEffect } from "react";


function AddAuto() {
    const [models, setModels] = useState([]);
    const [model, setModel] = useState('');
    const [color, setColor] = useState('');
    const [year, setYear] = useState('');
    const [vin, setVin] = useState('');


    const fetchData = async () => {
        const url = 'http://localhost:8100/api/models/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setModels(data.models);
        }
    };

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};

        data.color = color;
        data.vin = vin;
        data.year = year;
        data.model_id = model;




        const autoUrl = 'http://localhost:8100/api/automobiles/';
        const fetchOptions = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(autoUrl, fetchOptions);
        if (response.ok) {
            const newAuto = await response.json();


            setModel('');
            setYear('');
            setColor('');
            setVin('');
        }
    };

    const handleModelChange = (event) => {
        const value = event.target.value;
        setModel(value);
    }

    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    }

    const handleYearChange = (event) => {
        const value = event.target.value;
        setYear(value);
    }

    const handleVinChange = (event) => {
        const value = event.target.value;
        setVin(value);
    }




    useEffect(() => {
        fetchData();
    }, []);




    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add an automobile to inventory</h1>
                    <form onSubmit={handleSubmit} id="add-new-automobile-form">
                        <div className="form-floating mb-3">
                            <input value={color} onChange={handleColorChange} placeholder="Color" required type="text" id="color" name="color" className="form-control" />
                            <label htmlFor="color">Color...</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={year} onChange={handleYearChange} placeholder="year" required type="number" id="year" name="year" className="form-control" />
                            <label htmlFor="year">Year...</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={vin} onChange={handleVinChange} placeholder="vin" required type="text" id="vin" name="vin" className="form-control" />
                            <label htmlFor="vin">VIN...</label>
                        </div>
                        <div className="mb-3">
                            <select value={model} onChange={handleModelChange} placeholder="model" required id="model" name="model_id" className="form-select">
                                <option value="">Choose a model...</option>
                                {models.map(model => {
                                    return (
                                        <option key={model.id} value={model.id}>{model.name}</option>
                                    )
                                })}

                            </select>
                        </div>
                        <button className="btn btn-primary">Add</button>
                    </form>
                </div>
            </div>
        </div>
    );
}
export default AddAuto;
