import React, { useState, useEffect } from "react";

function ServiceHistory() {
    const [appointments, setAppointments] = useState([]);
    const [query, setQuery] = useState('')



    async function loadAppointments() {
        try {
            const response = await fetch('http://localhost:8080/api/appointments/');
            if (response.ok) {
                const data = await response.json();
                setAppointments(data.appointments)
            } else {
                console.error(response);
            }
        } catch (e) {
            console.error("No appointments found", e)
        }
    };

    useEffect(() => {
        loadAppointments();
    }, []);




    const filteredAppts = appointments.filter(appointment => {
        return appointment.vin.toLowerCase().includes(query.toLowerCase())
    })





    function dateConvert(date) {
        const dateTime = new Date(date);
        const dateFormtted = dateTime.toLocaleDateString();
        return dateFormtted;
    };

    function timeCovert(date) {
        const dateTime = new Date(date);
        const timeFormatted = dateTime.toLocaleTimeString([], { hour: '2-digit', minute: '2-digit' });
        return timeFormatted;
    };

    return (
        <div className="container">
            <h1>Service History</h1>
            <div className="mb-3">
                <input onChange={e => setQuery(e.target.value)} type="text" className="form-control" placeholder="Enter VIN" value={query} />
                <button className="btn btn-primary" >Search</button>
            </div>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Is VIP?</th>
                        <th>Customer</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    {filteredAppts.map((appointment) => (
                        <tr key={appointment.id}>
                            <td>{appointment.vin}</td>
                            <td>{appointment.vip ? 'Yes' : 'No'}</td>
                            <td>{appointment.customer}</td>
                            <td>{dateConvert(appointment.date_time)}</td>
                            <td>{timeCovert(appointment.date_time)}</td>
                            <td>
                                {appointment.technician.first_name} {appointment.technician.last_name}
                            </td>
                            <td>{appointment.reason}</td>
                            <td>{appointment.status}</td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    )
}

export default ServiceHistory;
