import React, { useState } from "react";

function AddTech() {
    const [firstName, setFirstname] = useState('');
    const [lastName, setLastname] = useState('');
    const [employeeId, setEmployeeid] = useState('');


    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};

        data.first_name = firstName;
        data.last_name = lastName;
        data.employee_id = employeeId;


        const techUrl = 'http://localhost:8080/api/technicians/'
        const fetchOptions = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },

        };
        const techResponse = await fetch(techUrl, fetchOptions);
        if (techResponse.ok) {
            const newTech = await techResponse.json();


            setFirstname('')
            setLastname('')
            setEmployeeid('')
        }
    };


    const handleFirstnameChange = (event) => {
        const value = event.target.value;
        setFirstname(value);
    }

    const handleLastnameChange = (event) => {
        const value = event.target.value;
        setLastname(value);
    }

    const handleEmployeeidChange = (event) => {
        const value = event.target.value;
        setEmployeeid(value);
    }





    return (
        <>
            <h1>Add a Technician</h1>
            <form onSubmit={handleSubmit} id="create-technician-form">
                <div className="form-floating mb-3">
                    <input value={firstName} onChange={handleFirstnameChange} placeholder="First name" required name="first_name" type="text" id="first_name" className='form-control' />
                    <label htmlFor="firstname">First Name...</label>
                </div>
                <div className="form-floating mb-3">
                    <input value={lastName} onChange={handleLastnameChange} placeholder="Last name" required name="last_name" type="text" id="last_name" className='form-control' />
                    <label htmlFor="lastname">Last Name...</label>
                </div>
                <div className="form-floating mb-3">
                    <input value={employeeId} onChange={handleEmployeeidChange} placeholder="Employee ID" required name="employee_id" type="number" id="employee_id" className='form-control' />
                    <label htmlFor="employeeid">Employee ID...</label>
                </div>
                <button className="btn btn-primary">Add</button>
            </form>
        </>
    );
}
export default AddTech;
