import React, { useState, useEffect } from "react";

function ListCustomers(props) {
  const [customers, setCustomers] = useState([]);

  const handleDeleteCustomer = async (event, id) => {
    event.preventDefault();

    const fetchOptions = {
      headers: {
        "Content-Type": "application/json",
      },
      method: "DELETE",
    };

    try {
      const response = await fetch(`http://localhost:8090/api/customers/${id}/`, fetchOptions);
      if (response.ok) {
        await loadCustomers();
      }
    } catch (error) {
      console.error(error);
    }
  };

  async function loadCustomers() {
    try {
      const response = await fetch('http://localhost:8090/api/customers/');
      if (response.ok) {
        const data = await response.json();
        setCustomers(data.customers);
      } else {
        console.error(response);
      }
    } catch (e) {
      console.error("No customers found", e);
    }
  }

  useEffect(() => {
    loadCustomers();
  }, []);

  return (
    <>
      <h1>Customers</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Phone Number</th>
            <th>Address</th>
            <th>Delete</th>
          </tr>
        </thead>
        <tbody>
          {customers.map((customer) => (
            <tr key={customer.id}>
              <td>{customer.first_name}</td>
              <td>{customer.last_name}</td>
              <td>{customer.phone_number}</td>
              <td>{customer.address}</td>
              <td>
                <a
                  onClick={(event) => handleDeleteCustomer(event, customer.id)}
                  type="button"
                  className="btn btn-link"
                >
                  Delete
                </a>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </>
  );
}

export default ListCustomers;
