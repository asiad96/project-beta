import React, { useEffect, useState } from 'react';

function CreateVehicleModel() {
  const [manufacturers, setManufacturers] = useState([]);
  const [modelName, setModelName] = useState('');
  const [pictureUrl, setPictureUrl] = useState('');
  const [manufacturerId, setManufacturerId] = useState('');

  const handleChangeModelName = (event) => {
    const value = event.target.value;
    setModelName(value);
  }

  const handleChangePictureUrl = (event) => {
    const value = event.target.value;
    setPictureUrl(value);
  }

  const handleChangeManufacturerId = (event) => {
    const value = event.target.value;
    setManufacturerId(value);
  }

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {
      name: modelName,
      picture_url: pictureUrl,
      manufacturer_id: manufacturerId,
    };

    const vehicleModelsURL = 'http://localhost:8100/api/models/';
    const fetchOptions = {
      method: 'POST',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    try {
      const response = await fetch(vehicleModelsURL, fetchOptions);
      if (response.ok) {
        setModelName('');
        setPictureUrl('');
        setManufacturerId('');
      }
    } catch (error) {
      console.error(error);
    }
  }

  const fetchData = async () => {
    const manufacturersURL = 'http://localhost:8100/api/manufacturers/';
    try {
      const response = await fetch(manufacturersURL);
      if (response.ok) {
        const data = await response.json();
        setManufacturers(data.manufacturers);
      }
    } catch (error) {
      console.error(error);
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <>
      <h1>Create a Vehicle Model</h1>
      <form onSubmit={handleSubmit} id="create-vehicle-model-form">
        <div className="form-floating mb-3">
          <input
            onChange={handleChangeModelName}
            value={modelName}
            placeholder="Model Name"
            required
            name="model_name"
            type="text"
            id="model_name"
            className="form-control"
          />
          <label htmlFor="model_name">Model Name</label>
        </div>
        <div className="form-floating mb-3">
          <input
            onChange={handleChangePictureUrl}
            value={pictureUrl}
            placeholder="Picture URL"
            required
            name="picture_url"
            type="text"
            id="picture_url"
            className="form-control"
          />
          <label htmlFor="picture_url">Picture URL</label>
        </div>
        <div className="form-floating mb-3">
          <div className="form-group">
            <select
              onChange={handleChangeManufacturerId}
              value={manufacturerId}
              placeholder="Manufacturer"
              required
              name="manufacturer_id"
              type="text"
              id="manufacturer_id"
              className="form-select"
            >
              <option value="">Select a manufacturer</option>
              {manufacturers.map((manufacturer) => (
                <option key={manufacturer.id} value={manufacturer.id}>
                  {manufacturer.name}
                </option>
              ))}
            </select>
          </div>
        </div>
        <button className="btn btn-primary">Create Model</button>
      </form>
    </>
  );

}

export default CreateVehicleModel;
