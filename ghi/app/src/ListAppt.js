import React, { useEffect, useState } from "react";


function ApptsList() {
    const [appointments, setAppointments] = useState([]);

    async function loadAppointments() {
        try {
            const response = await fetch('http://localhost:8080/api/appointments/');
            if (response.ok) {
                const data = await response.json();
                setAppointments(data.appointments || []);
            } else {
                console.error(response);
            }
        } catch (e) {
            console.error("No appointments found", e)
        }
    }

    useEffect(() => {
        loadAppointments()
    }, [])

    const handleFinish = async (event, id) => {
        event.preventDefault()

        const fetchOptions = {
            headers: {
                "Content-Type": "application/json",
            },
            method: "PUT",
            body: JSON.stringify({ status: "finished" }),
        }

        try {
            const response = await fetch(`http://localhost:8080/api/appointments/${id}/finish/`, fetchOptions);
            if (response.ok) {
                const data = await response.json();
                loadAppointments()


            }
        } catch (error) {
            console.error(error)
        }
    }

    const handleCancel = async (event, id) => {
        event.preventDefault()

        const fetchOptions = {
            headers: {
                "Content-Type": "application/json",
            },
            method: "PUT",
            body: JSON.stringify({ status: "canceled" }),
        }

        try {
            const response = await fetch(`http://localhost:8080/api/appointments/${id}/cancel/`, fetchOptions);
            if (response.ok) {
                const data = await response.json();
                loadAppointments()

            }
        } catch (error) {
            console.error(error)
        }
    }



    function dateConvert(date) {
        const dateTime = new Date(date);
        const dateFormtted = dateTime.toLocaleDateString();
        return dateFormtted;
    }

    function timeCovert(date) {
        const dateTime = new Date(date);
        const timeFormatted = dateTime.toLocaleTimeString([], { hour: '2-digit', minute: '2-digit' });
        return timeFormatted;
    }

    return (
        <><div className="container">
            <h1>Service Appointments</h1>
        </div><table className="table table-striped">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Is VIP?</th>
                        <th>Customer</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>

                    {appointments.map(appointment => {
                        if (appointment.status === "canceled" || appointment.status === "finished") {
                            return null;
                        }
                        return (
                            <tr key={appointment.id}>
                                <td>{appointment.vin}</td>
                                <td>{appointment.vip ? "Yes" : "No"}</td>
                                <td>{appointment.customer}</td>
                                <td>{dateConvert(appointment.date_time)}</td>
                                <td>{timeCovert(appointment.date_time)}</td>
                                <td>{appointment.technician.first_name} {appointment.technician.last_name}</td>
                                <td>{appointment.reason}</td>
                                <td><button onClick={(event) => handleCancel(event, appointment.id)} type="submit" className="btn btn-danger">Cancel</button></td>
                                <td><button onClick={(event) => handleFinish(event, appointment.id)} type="submit" className="btn btn-success">Finish</button></td>
                            </tr>
                        );
                    })}
                </tbody>
            </table></>

    );
};
export default ApptsList;
